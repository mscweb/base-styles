module.exports = function(grunt) {

  // Project configuration.

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    dss: {
      docs: {
        files: {
          'src/doc/': 'src/sass/components/*.{css,scss,sass,less,styl}'
        },
        options: {
          template_index: '../../../src/index.handlebars',
          parsers: {
            link: function(i, line, block){ var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig ; line.replace(exp, "<a href='$1'>$1</a>"); return line; }
          }
        }
      }
    }
  });

  // Load the Documented Style Sheets Parser
  grunt.loadNpmTasks('grunt-dss');

  // Default task(s).
  grunt.registerTask('default', ['dss']);

};