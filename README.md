# Base Styles for SUNY Morrisville Web Properties

A framework-agnostic starting point for styling web properties related to SUNY Morrisville.

Uses [grunt-dss](https://github.com/dsswg/grunt-dss) to generate styleguide documentation.

[Install with npm](https://www.npmjs.com/package/morrisville-base-styles):

`npm install morrisville-base-styles`

##Author
Lauren Saliba - [github](http://github.com/salibalr)
